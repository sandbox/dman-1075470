<?php
/**
 * @file
 * Node support for embedder
 */

/**
 * Return description of the available entities and their available formatters.
 */
function node_embedder_entities() {
  $entities = array();

  // User can select any available view mode.
  // Often full or teaser, but also any custom ones.
  $entity_info = entity_get_info('node');
  $view_modes = $entity_info['view modes'];
  // Unset some useless view modes.
  foreach (array('rss', 'search_index', 'search_result') as $unwanted) {
    unset($view_modes[$unwanted]);
  }
  $displays = array();
  foreach ($view_modes as $key => $mode) {
    $displays[$key] = $mode['label'];
  }

  // Note, key must be css-safe. lowercase, no spaces etc.
  $entities['node'] = array(
    'name' => 'Node',
    'parameters' => array(
      'nid' => 'Node ID',
      // view_mode.
      'display' => 'Display',
    ),
    'displays' => $displays,
    'defaults' => array(
      'display' => 'teaser',
    ),
    'fetch_callback' => 'node_embedder_fetch_element',
  );
  return $entities;
}

/**
 * Return the requested element data.
 *
 * Remember to try to enforce user access rights.
 */
function node_embedder_fetch_element($element_data, $element_element = NULL) {
  $element_info = node_embedder_entities();
  $element_data += $element_info['node']['defaults'];

  if (!isset($element_data['nid'])) {
    return "No node ID provided.";
  }

  $node = node_load($element_data['nid']);
  if (empty($node)) {
    return "No such node " . $element_data['nid'];
  }
  if (!node_access('view', $node)) {
    return "Permission denied, node unavailable.";
  }

  // Default view mode is teaser, if not set.
  $view_mode = isset($element_data['display']) ? $element_data['display'] : 'teaser';

  // If non-standard view_mode, verify the view mode is valid.
  if (!in_array($view_mode, array('full', 'teaser'))) {
    $entity_info = entity_get_info('node');
    $view_modes = $entity_info['view modes'];
    if (!isset($view_modes[$view_mode])) {
      return "Invalid View Mode " . $view_mode;
    }
  }

  $renderable = node_view($node, $view_mode);
  // Remove contextual links if they are here.
  unset($renderable['#contextual_links']);
  return drupal_render($renderable);
}
