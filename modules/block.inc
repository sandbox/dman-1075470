<?php
/**
 * @file
 * Block support for embedder
 */

/**
 * Return description of the available entities and their available formatters.
 */
function block_embedder_entities() {
  $entities = array();
  $entities['block'] = array(
    'name' => 'Block',
    'parameters' => array(
      'module' => 'Module',
      'delta' => 'Delta',
      'display' => 'Display',
    ),
    'displays' => array(
      'full' => 'Normal block markup',
      'content' => 'Content only',
    ),
    'defaults' => array(
      'display' => 'full',
      'region' => 'content',
      'title' => '',
    ),
    'fetch_callback' => 'block_embedder_fetch_element',
  );
  return $entities;
}

/**
 * Return the requested element data.
 *
 * Remember to try to enforce user access rights.
 */
function block_embedder_fetch_element($element_data, $element_element = NULL) {
  $element_info = block_embedder_entities();

  $element_data += $element_info['block']['defaults'];
  // We expect module+delta to be given.
  // Allow us to ask for block by block key as well.
  if (isset($element_data['block_key'])) {
    list($element_data['module'], $element_data['delta']) = preg_split("/_/", $element_data['block_key'], 2);
  }

  if (!isset($element_data['module'])) {
    return "No block module provided.";
  }
  if (empty($element_data['delta'])) {
    return "No block delta provided.";
  }
  // Optionally, we also may allow $element_data['display'] as a parameter.
  // Verify delta is valid. This is paranoia, for error-checking.
  $block_info = module_invoke($element_data['module'], 'block_info');
  if (empty($block_info[$element_data['delta']])) {
    return t("Module %module does not provide block %delta", array('%module' => $element_data['module'], '%delta' => $element_data['delta']));
  }

  // Prepare a block object for rendering.
  // System may expect any of the following parameters
  /*
  bid
  module *required
  delta *required
  theme
  status
  weight *unused
  region *default='content'
  custom
  visibility
  pages
  title *required WHY?
  cache
  subject *can be overridden
  */
  $block = (object) $element_data;

  // I COULD do a lot of block fetching by hand,
  // $array = module_invoke($block->module, 'block_view', $block->delta);
  // but if I let the system do it
  // it may take care of most of my access control worries, and caching.

  // Make a dummy list of blocks, as in a region (just one member).
  $block_key = "{$block->module}_{$block->delta}";
  $block_list[$block_key] = $block;

  // ACCESS CONTROL filters out bad blocks here.
  drupal_alter('block_list', $block_list);
  // However, some block utile like to load the current node which can
  // trigger recursion. This is throttled in embedder_fetch_element_html()

  if (empty($block_list[$block_key])) {
    return '';
  }

  // Get core to load and prepare them.
  $list = _block_render_blocks($block_list);
  // Content should be loaded now.

  switch ($element_data['display']) {
    case 'content':
      // Display just the content.
      // _block_render_blocks shifted content into #markup
      return $block_list[$block_key]->content['#markup'];

    case 'full':
    default:
      // Get core to set it up for full rendering.
      $renderable = _block_get_renderable_array($list);
      // Remove contextual links if they are here.
      unset($renderable['#contextual_links']);
      return drupal_render($renderable);
  }
}
