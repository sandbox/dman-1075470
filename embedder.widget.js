(function ($) {
/**
 * Support for the editing widget.
 * Object browser, selector and preview used as a popup.
 */

  // Define the embedderwidget object.
  Drupal.embedderwidget = Drupal.embedderwidget || {};

  // Our 'global' variable for the dialog jQuery object.
  // Must get attached on startup.
  Drupal.embedderwidget.$dialog = null;

  /**
   * Display the Drupal throbber.
   *
   * @param element
   *  The jQuery element to append the throbber to.
   * @param {bool} throbber
   *  If TRUE, then display the throbber element. False to remove it again.
   */
  Drupal.embedderwidget.throbber = function(element, throbber) {
    if (throbber) {
      element.after('<div class="ahah-progress ahah-progress-throbber"><div class="throbber">&nbsp;</div></div>');
    }
    else {
      element.siblings('.ahah-progress').remove();
    }
  };

  /**
   * Helper to find the element that's highlighted or being edited.
   *
   * If you click inside one, the selectin expands to the whole thing.
   * Returns null if not an existing one.
   */
  Drupal.embedderwidget.getSelectedEmbed = function(editor) {
    try {
      if (! editor) {
        editor = CKEDITOR.currentInstance;
      }
      var selection = editor.getSelection();
      prev = selection.getSelectedText();
      var node = selection.getStartElement().$;
      // currentElement is a jquery handle
      var currentElement = $(node).closest('.embedder');
      if (! currentElement) {
        return NULL;
      }
      // element should be a ckeditor handle.
      element = new CKEDITOR.dom.node(currentElement.get(0));
      editor.getSelection().selectElement( element );

      // Not sure, but it seems that selecting the element actually selects
      // the *content* of the element, so misses the wrapper.
      // This becomes a problem later when we try to replace it.

      /*
        debugging : what is currently selected in the wysiwyg
        jQuery('iframe').contents().find('body').get(0).ownerDocument.getSelection()
       */

      return currentElement;
   }
    catch( e ) {
      console.log('Had trouble selecting an existing embedder element.' + e.message);
      return null;
    }
  };

  /**
   * Helper function to build the browser.
   *
   * I failed to pass any useful context from
   * Drupal.wysiwyg.plugins['embedder'].invoke()
   * so, no params. Figure out the context we need from globals I guess.
   *
   * @param instanceId
   *   Identify the wysiwyg editor we are coming from.
   * @param plugin
   *   The Drupal.wysiwyg.plugins['embedder'] instance that invoked me.
   */
  Drupal.embedderwidget.dialog = function(editorName, plugin) {

    var editor = Drupal.wysiwyg.instances[editorName];

    // A jquery (not a ckeditor) element.
    this.currentElement = this.getSelectedEmbed();
    this.currentElementParameters = this.currentElement ? this.currentElement.data() : {};
    // The currentElementParameters are now like
    // {element: "node", nid: 1, display: "teaser"}

    console.log('Setting up the dialog box');
    console.log(this.currentElementParameters);
    // Create our browser dialog.
    Drupal.embedderwidget.$dialog = $('<div id="embedderwidget-dialog"></div>')
      .dialog({
        autoopen: true,
        modal: true,
        width: 640,
        height: 480,
        title: 'Embed object',
        buttons: { 
          'Cancel': function(){ $(this).dialog("close"); } ,
          'OK': function(){ Drupal.embedderwidget.dialogOK(); } 
        }
      })
      // Remove the dialog entirely from the DOM after closing.
      .bind( "dialogclose", function(event, ui) {
        $(this).remove();
      })
      // Store contextual parameters,
      // information about the object we were on when we opened
      .data('editorName', editorName)
      // Store the referenced element to the dialog jquery data.
      .data('currentElement', this.currentElement)

    // Grab the form that we will use inside the browser
    Drupal.embedderwidget.throbber(Drupal.embedderwidget.$dialog, true);

    // We post the parameters (element type, id etc) found in the
    // data- attributtes back with the form. These args should inform the
    // callback about the current element to retrieve.
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: Drupal.settings.embedder.embedderBrowserUrl,
      data: { js: true, embed: this.currentElementParameters },
      success: function(data, textStatus, XMLHttpRequest) {
        Drupal.embedderwidget.throbber(Drupal.embedderwidget.$dialog, false);
        if (data.status) {
          // Open the browser.
          Drupal.embedderwidget.$dialog.html(data.data);
          Drupal.embedderwidget.$dialog.dialog('open');
          // Attach any required behaviors to the browser.
          Drupal.attachBehaviors(Drupal.embedderwidget.$dialog, null);
          console.log(Drupal.embedderwidget.$dialog);
        }
        else {
          // Failure...
          alert(Drupal.t('Unknown error in Drupal.embedderwidget.dialog.', null, null));
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // Failure...
        Drupal.embedderwidget.throbber(Drupal.embedderwidget.$dialog, false);
        alert(Drupal.t('Error in Drupal.embedderwidget.dialog: @error', { '@error': textStatus }, null));
      }
    });
  };
  
  /**
   * When some values on the widget form have changed, fetch the preview
   */
  Drupal.embedderwidget.updatePreview = function() {
    // Read values from the current form
    var $dialog = Drupal.embedderwidget.$dialog;
    var $element_type = $('#edit-element', $dialog).val();
    var form_data = {};
    $('input,select', $dialog).each(
      function() {
        form_data[this.name] = $(this).val();
      }
    );
    var post_data = form_data;
    post_data['js'] = true;
    
    // Post the form data to the preview URL and display the result
    
    Drupal.embedderwidget.throbber($('#embedder-preview', $dialog), true);
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: Drupal.settings.embedder.embedderPreviewUrl,
      data: post_data,
      success: function(data, textStatus, XMLHttpRequest) {
        Drupal.embedderwidget.throbber($('#embedder-preview', $dialog), false);
        if (data.status) {
          // Show the result.
          $('#embedder-preview', $dialog).html(data.data);
          // Change its click behavior
          
          // Attach any required behaviors to the browser.
          Drupal.attachBehaviors(Drupal.embedderwidget.$dialog, null);
        }
        else {
          // Failure...
          alert(Drupal.t('Unknown error in Drupal.embedderwidget.dialog.', null, null));
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // Failure...
        Drupal.embedderwidget.throbber(Drupal.embedderwidget.$dialog, false);
        alert(Drupal.t('Error in Drupal.embedderwidget.dialog: @error  @response', { '@error': textStatus, '@response' : XMLHttpRequest.responseText }, null));
      }
    });
  };


  /**
   * Clicking the OK on the dialog will either insert the new data into the field,
   */
  Drupal.embedderwidget.dialogOK = function() {
    var currentPreviewHTML = $('#embedder-preview', Drupal.embedderwidget.$dialog).html();

    if (currentPreviewHTML) {
      // Simply insert our node into the field or WYSIWYG browser.
      // The insert() function should handle replacing the current selection.
      editorName = this.$dialog.data('editorName')
      editor = Drupal.wysiwyg.instances[editorName];

      if (! this.currentElement) {
        editor.insert(currentPreviewHTML);
      }
      else {
        // Eomething goes wrong when trying to insert over top of
        // existing selection using editor.insert(). The selected text
        // does not seem to include the wrapper div, and we end up nesting things.
        // Use DOM instead here, and just replace the old element with the new.
        $(this.currentElement).replaceWith(currentPreviewHTML);
        // not the ckeditor way, but the ckeditor way don't work right.
      }

      /*
      editor.getSelection().selectElement( this.currentElement );
      editor.insertElement( $(currentPreviewHTML) );
      */

      // Close the dialog.
      Drupal.embedderwidget.$dialog.dialog('close');
      Drupal.embedderwidget.$dialog.remove();
    }
  };

  /** 
   * push the new data back to the caller
   */
  Drupal.embedderwidget.insert = function(replacementText) {

    // Change the value of the associated field, and ensure we
    // run the correct behaviors again.
    var $field = $(Drupal.embedderwidget.$dialog.data('element'));
    $field.val(replacementText);
    // .removeClass('nrembrowserReplaceTextfields-processed');

    // Run the behaviors on the new thumbnail.
    Drupal.attachBehaviors($field.parent().parent(), null);
  };

  
  Drupal.behaviors.embedderwidget = {
    attach : function(context) {
      
      // Attach onchange to all fields to update the preview
      $('#embedder-object-form').once('trigger-preview-change', function() {
        console.log('Setting up embedder preview trigger');
        $('input, select', this).change( function() {
            Drupal.embedderwidget.updatePreview();
        });
      });
      
      // If called out of pop-up context, the 'dialog' - which is normally
      // the pop-up, will just be the in-page form
      // No, we need another wrapper to support this
      // Drupal.embedderwidget.$dialog = $('#embedder-object-form');
    }
  };
  
  
})(jQuery);
