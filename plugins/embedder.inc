<?php
// $Id: embedder.inc,v 1.1 2011/02/13 13:22:35 dman Exp $

/**
 * @file
 * Wysiwyg API integration for embedder elements.
 */

/**
 * Implementation of <module_name>_<plugin_name>_plugin().
 */
function embedder_embedder_plugin() {
  $plugins['embedder'] = array(
    'title' => t('Embeds'),
    'vendor url' => 'http://drupal.org/project/embedder',
    'icon file' => 'embedder-icon.gif',
    'icon title' => t('Insert a dynamic element'),
    'settings' => array(),
  );
  return $plugins;
}
