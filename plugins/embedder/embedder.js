// Thanks to http://deglos.com/blog/2010/12/21/building-wysiwyg-plugin-drupal
(function ($) {

Drupal.wysiwyg.plugins['embedder'] = {

  /**
   * Return whether the passed node belongs to this plugin.
   *
   * @param node
   *   The currently focused DOM element in the editor content.
   */
  isNode: function(node) {
    // Used to tell the wysiwyg whether we want to take responsibility
    // for actions on this node.

    // Most of the time you can't click directly on the container div,
    // so need to search context to see if you clicked *inside* one.

    // Get TextNode if node is empty (clicked inside the element, not on it).
    if (node == null) {
      var editor = CKEDITOR.currentInstance;
      var selection = new CKEDITOR.dom.selection(editor.document);
      node = selection.getStartElement().$;
    }

    // Is self or parents an embedder.
    return $(node).closest('.embedder').length;
  },


  /**
   * Execute the button - insert an embedder div.
   *
   * Invoke gets triggered somewhere inside the ckeditor.init() routine.
   *
   * Invoke is called when the toolbar button is clicked.
   * The settings dialog should open.
   *
   * @param editorName the ckeditor id
   */
  invoke: function(data, pluginSettings, editorName) {


    // I guess we should be doing some conversion from Drupalisms to
    // ckeditorisms or something here, but I can't distinguish
    // between the two yet.
    // So just launch the dialog with some context info and go from there.

    // There was nothing useful in the data or settings I'm given?

    // var editor = Drupal.wysiwyg.instances[instanceId];
    /*
    if (editor.addMenuItems) {
      // Try to insert the contextmenu.
      editor.addMenuItems({
        'embedder' : {
          label : 'Insert embed',
          command : 'insertEmbed'
        }
      });
    }
    */

    // Pass through info about the context we are launcing from.
    $('#' + editorName)
      .data('editorName', editorName);

    // Launch the browser widget.

    // What is the currently selected element?
    // Why don't I know?
    // It will have to look at the currently selected thing and extract the
    // current attributes out of it.
    Drupal.embedderwidget.dialog(editorName, this);
  },
  
  /**
   * I do NOT do anything with attach and detach calls,
   * My markup is round-trip safe! Because I am cool.
   */

  /**
   * Helper function to return a HTML placeholder.
   */
  _getPlaceholder: function (settings) {
    var content = '<div class="embedder" >Dynamic embed!</div>';
    return content;
  }
  
};

})(jQuery);
