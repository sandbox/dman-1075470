(function ($) {

/**
 * @file
 * UI addition to enhance the import source switcher
 *
 */
Drupal.behaviors.filtering_fieldset = {
  attach : function(context) {
    $('.filtering-fieldset', context).once('filtering-fieldset', function () {
      console.log('Setting up filtered fieldset');
      var container = $(this);
      // $(this).addClass('filtered');
      $('.filtering-selector', container)
        .addClass('filtering-trigger')
        .change(
          /*
          When the select changes, we change the class of the containing fieldset.
          and hide/show all the contents depending on what was selected.
          */
          function() {
            $('.filtered-fieldset', container).hide().removeClass('filter-selected');
            console.log('filtering to show ' + $(this).val());
            if ($(this).val()) {
              $('.filtered-fieldset-' + $(this).val(), container).show().addClass('filter-selected');
            }
          }
        );
      // Trigger the filter to update the current display
      $('.filtering-selector', container).trigger('change');
      console.log('Fieldset filtered');
    }) //once function
  } //attach
} // behavior

})(jQuery);
